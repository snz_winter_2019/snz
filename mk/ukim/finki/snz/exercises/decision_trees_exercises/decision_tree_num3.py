from mk.ukim.finki.snz.lab.decision_trees_lab.decision_tree_learning import build_tree, classify

training_data = [['slashdot', 'USA', 'yes', 18, 'None'],
                 ['google', 'France', 'yes', 23, 'Premium'],
                 ['google', 'France', 'yes', 23, 'Basic'],
                 ['google', 'France', 'yes', 23, 'Basic'],
                 ['digg', 'USA', 'yes', 24, 'Basic'],
                 ['kiwitobes', 'France', 'yes', 23, 'Basic'],
                 ['google', 'UK', 'no', 21, 'Premium'],
                 ['(direct)', 'New Zealand', 'no', 12, 'None'],
                 ['(direct)', 'UK', 'no', 21, 'Basic'],
                 ['google', 'USA', 'no', 24, 'Premium'],
                 ['slashdot', 'France', 'yes', 19, 'None'],
                 ['digg', 'USA', 'no', 18, 'None'],
                 ['google', 'UK', 'no', 18, 'None'],
                 ['kiwitobes', 'UK', 'no', 19, 'None'],
                 ['digg', 'New Zealand', 'yes', 12, 'Basic'],
                 ['slashdot', 'UK', 'no', 21, 'None'],
                 ['google', 'UK', 'yes', 18, 'Basic'],
                 ['kiwitobes', 'France', 'yes', 19, 'Basic']]

if __name__ == "__main__":
    referrer = input()
    location = input()
    read_FAQ = input()
    pages_visited = int(input())
    service_chosen = input()

    test_case = [referrer, location, read_FAQ, pages_visited, service_chosen]

    t = build_tree(training_data)
    items = classify(test_case, t)
    potencial_solutions = list(sorted([*items.keys()], key=items.get, reverse=True))
    print(potencial_solutions[0])

