from mk.ukim.finki.snz.lab.decision_trees_lab.decision_tree_learning import build_tree, print_tree

trainingData=[['slashdot','USA','yes',18,'None'],
        ['google','France','yes',23,'Premium'],
        ['google','France','yes',23,'Basic'],
        ['google','France','yes',23,'Basic'],
        ['digg','USA','yes',24,'Basic'],
        ['kiwitobes','France','yes',23,'Basic'],
        ['google','UK','no',21,'Premium'],
        ['(direct)','New Zealand','no',12,'None'],
        ['(direct)','UK','no',21,'Basic'],
        ['google','USA','no',24,'Premium'],
        ['slashdot','France','yes',19,'None'],
        ['digg','USA','no',18,'None'],
        ['google','UK','no',18,'None'],
        ['kiwitobes','UK','no',19,'None'],
        ['digg','New Zealand','yes',12,'Basic'],
        ['slashdot','UK','no',21,'None'],
        ['google','UK','yes',18,'Basic'],
        ['kiwitobes','France','yes',19,'Basic']]


if __name__ == "__main__":
    referrer = input()
    location = input()
    readFAQ = input()
    pagesVisited = int(input())
    serviceChosen = input()

    testCase = [referrer, location, readFAQ, pagesVisited, serviceChosen]
    trainingData.append(testCase)
    t = build_tree(trainingData)
    print_tree(t)
